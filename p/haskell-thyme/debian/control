Source: haskell-thyme
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders: Clint Adams <clint@debian.org>
Priority: optional
Section: haskell
Rules-Requires-Root: no
Build-Depends: debhelper (>= 10),
 haskell-devscripts-minimal | haskell-devscripts (>= 0.8),
 cdbs,
 ghc (>= 8.4.3),
 ghc-prof,
 libghc-quickcheck2-dev (>= 2.4),
 libghc-quickcheck2-prof (>= 2.4),
 libghc-aeson-dev (>= 0.6),
 libghc-aeson-prof (>= 0.6),
 libghc-attoparsec-dev (>= 0.10),
 libghc-attoparsec-prof (>= 0.10),
 libghc-old-locale-dev (>= 1.0),
 libghc-old-locale-prof (>= 1.0),
 libghc-profunctors-dev (>= 3.1.2),
 libghc-profunctors-prof (>= 3.1.2),
 libghc-random-dev,
 libghc-random-prof,
 libghc-vector-dev (>= 0.9),
 libghc-vector-prof (>= 0.9),
 libghc-vector-space-dev (>= 0.8),
 libghc-vector-space-prof (>= 0.8),
 libghc-vector-th-unbox-dev (>= 0.2.1.0),
 libghc-vector-th-unbox-prof (>= 0.2.1.0),
 libghc-quickcheck2-dev,
 libghc-attoparsec-dev,
 libghc-old-locale-dev,
 libghc-profunctors-dev,
 libghc-vector-space-dev,
Build-Depends-Indep: ghc-doc,
 libghc-quickcheck2-doc,
 libghc-aeson-doc,
 libghc-attoparsec-doc,
 libghc-old-locale-doc,
 libghc-profunctors-doc,
 libghc-random-doc,
 libghc-vector-doc,
 libghc-vector-space-doc,
 libghc-vector-th-unbox-doc,
Standards-Version: 4.5.0
Homepage: https://github.com/liyang/thyme
X-Description: faster time library
 Thyme is a rewrite of the fine time library, with a particular focus
 on performance for applications that make heavy use of timestamps. For
 example, UTCTime is represented with μs precision as an Int64, which
 gives a usable range from -290419-11-07 19:59:05.224192 UTC to
 294135-11-26 04:00:54.775807 UTC in the future.
 .
 Conversions are provided as Iso's from the lens package, while
 AdditiveGroup, VectorSpace and AffineSpace from vector-space allow for
 more principled operations instead of Num, Fractional & al.
 .
 Thyme uses strict and unpacked tuples throughout, e.g. YearMonthDay or
 WeekDate. Descriptive Int synonyms such as Year and DayOfMonth are
 also provided.
 .
 On platforms where Int is 64-bits wide, types with an Enum instance
 can be used as Keys for IntMap, preferably via the EnumMap wrapper
 provided by enummapset-th. In any case the Ord instances are much
 faster, if you must use Map.
 .
 Data.Thyme.Time is a drop-in compatibility module for existing code.

Package: libghc-thyme-dev
Architecture: any
Depends: ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends: ${haskell:Recommends},
Suggests: ${haskell:Suggests},
Conflicts: ${haskell:Conflicts},
Provides: ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-thyme-prof
Architecture: any
Depends: ${haskell:Depends},
 ${misc:Depends},
Recommends: ${haskell:Recommends},
Suggests: ${haskell:Suggests},
Conflicts: ${haskell:Conflicts},
Provides: ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-thyme-doc
Architecture: all
Section: doc
Depends: ${haskell:Depends},
 ${misc:Depends},
Recommends: ${haskell:Recommends},
Suggests: ${haskell:Suggests},
Conflicts: ${haskell:Conflicts},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}
