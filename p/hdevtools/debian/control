Source: hdevtools
Section: devel
Priority: optional
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders: Ilias Tsitsimpis <i.tsitsimpis@gmail.com>
Rules-Requires-Root: no
Build-Depends: debhelper (>= 10)
  , haskell-devscripts (>= 0.13)
  , cdbs
  , ghc (>= 8)
  , ghc-ghci
  , libghc-cmdargs-dev
  , libghc-ghc-paths-dev
  , libghc-syb-dev
  , libghc-network-dev
Standards-Version: 4.1.4
Homepage: http://hackage.haskell.org/package/hdevtools
Vcs-Browser: https://salsa.debian.org/haskell-team/DHG_packages/tree/master/p/hdevtools
Vcs-Git: https://salsa.debian.org/haskell-team/DHG_packages.git [p/hdevtools]

Package: hdevtools
Architecture: any
Depends: ${shlibs:Depends}
  , ${misc:Depends}
  , ${haskell:ghc-version}
Description: GHC powered daemon for fast Haskell development
 hdevtools is a backend for text editor plugins, to allow for things such as
 syntax and type checking of Haskell code, and retrieving type information, all
 directly from within your text editor.
 .
 The advantage that hdevtools has over ghc-mod is that it runs silently
 in a persistent background process, and therefore is able to keeps all of your
 Haskell modules and dependent libraries loaded in memory. This way, when you
 change only a single source file, only it needs to be reloaded and rechecked,
 instead of having to reload everything.
 .
 This makes hdevtools very fast for checking syntax and type errors (runs
 just as fast as the ":reload" command in GHCi).
 .
 In fact, syntax and type checking is so fast, that you can safely enable auto
 checking on every save. Even for huge projects, checking is nearly instant.
 .
 In addition to checking Haskell source code for errors, `hdevtools` has tools
 for getting info about identifiers, and getting type information for snippets
 of code.
