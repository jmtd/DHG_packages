Source: haskell-github
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Priority: optional
Section: haskell
Rules-Requires-Root: no
Build-Depends:
 cdbs,
 debhelper (>= 10),
 haskell-devscripts (>= 0.15),
 ghc (>= 8.4.3),
 ghc-prof,
 libghc-aeson-dev (>= 1.4.0.0),
 libghc-aeson-dev (<< 1.5),
 libghc-aeson-prof,
 libghc-base-compat-dev (>= 0.10.4),
 libghc-base-compat-dev (<< 0.12),
 libghc-base-compat-prof,
 libghc-base16-bytestring-dev (<< 0.2),
 libghc-base16-bytestring-dev (>= 0.1.1.6),
 libghc-base16-bytestring-prof,
 libghc-binary-instances-dev (>= 1),
 libghc-binary-instances-dev (<< 1.1),
 libghc-binary-instances-prof,
 libghc-cryptohash-sha1-dev (>= 0.11.100.1),
 libghc-cryptohash-sha1-dev (<< 0.12),
 libghc-cryptohash-sha1-prof,
 libghc-deepseq-generics-dev (>= 0.2.0.0),
 libghc-deepseq-generics-dev (<< 0.3),
 libghc-deepseq-generics-prof,
 libghc-exceptions-dev (>= 0.10.2),
 libghc-exceptions-dev (<< 0.11),
 libghc-exceptions-prof,
 libghc-hashable-dev (>= 1.2.7.0),
 libghc-hashable-dev (<< 1.4),
 libghc-hashable-prof,
 libghc-http-client-dev (>= 0.5.12),
 libghc-http-client-dev (<< 0.7),
 libghc-http-client-prof,
 libghc-http-client-tls-dev (>= 0.3.5.3),
 libghc-http-client-tls-dev (<< 0.4),
 libghc-http-client-tls-prof,
 libghc-http-link-header-dev (>= 1.0.3.1),
 libghc-http-link-header-dev (<< 1.1),
 libghc-http-link-header-prof,
 libghc-http-types-dev (>= 0.12.3),
 libghc-http-types-dev (<< 0.13),
 libghc-http-types-prof,
 libghc-iso8601-time-dev (>= 0.1.5),
 libghc-iso8601-time-dev (<< 0.2),
 libghc-iso8601-time-prof,
 libghc-network-uri-dev (>= 2.6.1.0),
 libghc-network-uri-dev (<< 2.7),
 libghc-network-uri-prof,
 libghc-tagged-dev (>= 0.8.5),
 libghc-tagged-dev (<< 0.9),
 libghc-tagged-prof,
 libghc-tls-dev (>= 1.4.1),
 libghc-tls-prof,
 libghc-transformers-compat-dev (>= 0.6.5),
 libghc-transformers-compat-dev (<< 0.7),
 libghc-transformers-compat-prof,
 libghc-unordered-containers-dev (>= 0.2.10.0),
 libghc-unordered-containers-dev (<< 0.3),
 libghc-unordered-containers-dev (>= 0.2),
 libghc-unordered-containers-prof,
 libghc-vector-dev (>= 0.12.0.1),
 libghc-vector-dev (<< 0.13),
 libghc-vector-prof,
 libghc-vector-instances-dev (>= 3.4),
 libghc-vector-instances-dev (<< 3.5),
 libghc-vector-instances-prof,
 libghc-aeson-dev,
 libghc-base-compat-dev,
 libghc-file-embed-dev,
 libghc-file-embed-prof,
 libghc-hspec-dev (>= 2.6.1),
 libghc-hspec-dev (<< 2.8),
 libghc-hspec-prof,
 libghc-tagged-dev,
 libghc-unordered-containers-dev,
 libghc-vector-dev,
Build-Depends-Indep: ghc-doc,
 libghc-aeson-doc,
 libghc-base-compat-doc,
 libghc-base16-bytestring-doc,
 libghc-binary-instances-doc,
 libghc-cryptohash-sha1-doc,
 libghc-deepseq-generics-doc,
 libghc-exceptions-doc,
 libghc-hashable-doc,
 libghc-http-client-doc,
 libghc-http-client-tls-doc,
 libghc-http-link-header-doc,
 libghc-http-types-doc,
 libghc-iso8601-time-doc,
 libghc-network-uri-doc,
 libghc-tagged-doc,
 libghc-tls-doc,
 libghc-transformers-compat-doc,
 libghc-unordered-containers-doc,
 libghc-vector-doc,
 libghc-vector-instances-doc,
Standards-Version: 4.4.0
Homepage: https://github.com/phadej/github
Vcs-Browser: https://salsa.debian.org/haskell-team/DHG_packages/tree/master/p/haskell-github
Vcs-Git: https://salsa.debian.org/haskell-team/DHG_packages.git [p/haskell-github]

Package: libghc-github-dev
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: Haskell Github API${haskell:ShortBlurb}
 The Github API provides programmatic access to the full Github Web site,
 from Issues to Gists to repos down to the underlying git data like
 references and trees. This library wraps all of that, exposing a basic
 but Haskell-friendly set of functions and data structures.
 .
 ${haskell:Blurb}

Package: libghc-github-prof
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: Haskell Github API${haskell:ShortBlurb}
 The Github API provides programmatic access to the full Github Web site,
 from Issues to Gists to repos down to the underlying git data like
 references and trees. This library wraps all of that, exposing a basic
 but Haskell-friendly set of functions and data structures.
 .
 ${haskell:Blurb}

Package: libghc-github-doc
Architecture: all
Section: doc
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Description: Haskell Github API${haskell:ShortBlurb}
 The Github API provides programmatic access to the full Github Web site,
 from Issues to Gists to repos down to the underlying git data like
 references and trees. This library wraps all of that, exposing a basic
 but Haskell-friendly set of functions and data structures.
 .
 ${haskell:Blurb}
