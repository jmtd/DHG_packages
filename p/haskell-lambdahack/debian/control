Source: haskell-lambdahack
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders: Clint Adams <clint@debian.org>, Mikolaj Konarski <mikolaj.konarski@funktory.com>
Priority: optional
Section: haskell
Rules-Requires-Root: no
Build-Depends: debhelper (>= 10),
 haskell-devscripts (>= 0.8),
 cdbs,
 ghc (>= 8.4.3),
 ghc-prof,
 libghc-assert-failure-dev (>= 0.1.2),
 libghc-assert-failure-dev (<< 0.2),
 libghc-assert-failure-prof,
 libghc-async-dev (>= 2),
 libghc-async-prof,
 libghc-base-compat-dev (>= 0.8.0),
 libghc-base-compat-prof,
 libghc-enummapset-dev (>= 0.5.2.2),
 libghc-enummapset-prof,
 libghc-hashable-dev (>= 1.1.2.5),
 libghc-hashable-prof,
 libghc-hsini-dev (>= 0.2),
 libghc-hsini-prof,
 libghc-keys-dev (>= 3),
 libghc-keys-prof,
 libghc-miniutter-dev (>= 0.5.0.0),
 libghc-miniutter-prof,
 libghc-optparse-applicative-dev (>= 0.13),
 libghc-optparse-applicative-prof,
 libghc-pretty-show-dev (>= 1.6),
 libghc-pretty-show-prof,
 libghc-primitive-dev (>= 0.6.1.0),
 libghc-primitive-prof,
 libghc-random-dev (>= 1.1),
 libghc-random-prof,
 libghc-sdl2-dev (>= 2),
 libghc-sdl2-prof,
 libghc-sdl2-ttf-dev (>= 2),
 libghc-sdl2-ttf-prof,
 libghc-unordered-containers-dev (>= 0.2.3),
 libghc-unordered-containers-prof,
 libghc-vector-dev (>= 0.11),
 libghc-vector-prof,
 libghc-vector-binary-instances-dev (>= 0.2.3.1),
 libghc-vector-binary-instances-prof,
 libghc-zlib-dev (>= 0.5.3.1),
 libghc-zlib-prof,
Build-Depends-Indep: ghc-doc,
 libghc-assert-failure-doc,
 libghc-async-doc,
 libghc-base-compat-doc,
 libghc-enummapset-doc,
 libghc-hashable-doc,
 libghc-hsini-doc,
 libghc-keys-doc,
 libghc-miniutter-doc,
 libghc-optparse-applicative-doc,
 libghc-pretty-show-doc,
 libghc-primitive-doc,
 libghc-random-doc,
 libghc-sdl2-doc,
 libghc-sdl2-ttf-doc,
 libghc-unordered-containers-doc,
 libghc-vector-doc,
 libghc-vector-binary-instances-doc,
 libghc-zlib-doc,
Standards-Version: 4.6.0
Homepage: https://github.com/LambdaHack/LambdaHack
Vcs-Browser: https://salsa.debian.org/haskell-team/DHG_packages/tree/master/p/haskell-lambdahack
Vcs-Git: https://salsa.debian.org/haskell-team/DHG_packages.git [p/haskell-lambdahack]
X-Description: tactical squad ASCII roguelike dungeon crawler game engine
 LambdaHack is a Haskell game engine library for ASCII roguelike
 games of arbitrary theme, size and complexity, with optional
 tactical squad combat. It's packaged together with a sample
 dungeon crawler in fantasy setting that can be tried out
 as a native binary or in the browser at http://lambdahack.github.io.
 .
 Please see the changelog file for recent improvements
 and the issue tracker for short-term plans. Long term goals
 include multiplayer tactical squad combat, in-game content
 creation, auto-balancing and persistent content modification
 based on player behaviour. Contributions are welcome.

Package: libghc-lambdahack-dev
Architecture: any
Depends: ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends: ${haskell:Recommends},
Suggests: ${haskell:Suggests},
Conflicts: ${haskell:Conflicts},
Provides: ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-lambdahack-prof
Architecture: any
Depends: ${haskell:Depends},
 ${misc:Depends},
Recommends: ${haskell:Recommends},
Suggests: ${haskell:Suggests},
Conflicts: ${haskell:Conflicts},
Provides: ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-lambdahack-doc
Architecture: all
Section: doc
Depends: ${haskell:Depends},
 ${misc:Depends},
Recommends: ${haskell:Recommends},
Suggests: ${haskell:Suggests},
Conflicts: ${haskell:Conflicts},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: lambdahack
Architecture: any
Section: games
Depends: ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends: ${haskell:Recommends},
Suggests: ${haskell:Suggests},
Conflicts: ${haskell:Conflicts},
Provides: ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}
