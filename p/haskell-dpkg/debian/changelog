haskell-dpkg (0.0.3-12) unstable; urgency=medium

  [ Clint Adams ]
  * Set Rules-Requires-Root to no.

  [ Ilias Tsitsimpis ]
  * Bump debhelper compat level to 10

 -- Ilias Tsitsimpis <iliastsi@debian.org>  Sun, 30 Sep 2018 21:07:10 +0300

haskell-dpkg (0.0.3-11) unstable; urgency=medium

  [ Ilias Tsitsimpis ]
  * Change Priority to optional. Since Debian Policy version 4.0.1,
    priority extra has been deprecated.
  * Use the HTTPS form of the copyright-format URL
  * Modify d/watch and Source field in d/copyright to use HTTPS
  * Declare compliance with Debian policy 4.1.1
  * Use salsa.debian.org URLs in Vcs-{Browser,Git} fields

  [ Clint Adams ]
  * Bump to Standards-Version 4.1.4.

 -- Clint Adams <clint@debian.org>  Mon, 09 Apr 2018 20:04:48 -0400

haskell-dpkg (0.0.3-10) unstable; urgency=medium

  * Upload to unstable as part of GHC 8 transition.

 -- Clint Adams <clint@debian.org>  Thu, 27 Oct 2016 18:33:28 -0400

haskell-dpkg (0.0.3-9) experimental; urgency=medium

  [ Dmitry Bogatov ]
  * Use secure (https) uri in Vcs-Git field in 'debian/control'
  * Bump standards version to 3.9.8 (no changes needed)
  * Convert `debian/copyright' to dep5 format

  [ Clint Adams ]
  * Temporarily build-depend on ghc 8.

 -- Clint Adams <clint@debian.org>  Mon, 17 Oct 2016 01:05:04 -0400

haskell-dpkg (0.0.3-8) unstable; urgency=medium

  * Switch Vcs-Git/Vcs-Browser headers to new location.

 -- Clint Adams <clint@debian.org>  Thu, 03 Dec 2015 14:54:23 -0500

haskell-dpkg (0.0.3-7) experimental; urgency=medium

  * Bump standards-version to 3.9.6
  * Depend on haskell-devscripts >= 0.10 to ensure that this package
    builds against GHC in experimental

 -- Joachim Breitner <nomeata@debian.org>  Thu, 20 Aug 2015 10:27:31 +0200

haskell-dpkg (0.0.3-6) unstable; urgency=medium

  * Upload to unstable

 -- Joachim Breitner <nomeata@debian.org>  Mon, 27 Apr 2015 11:49:21 +0200

haskell-dpkg (0.0.3-5) experimental; urgency=low

  * Adjust watch file to new hackage layout
  * Depend on haskell-devscripts 0.9, found in experimental
  * Build with --disable-shared, as libdpkg-dev can only be used to build
    static libraries, it seems.

 -- Joachim Breitner <nomeata@debian.org>  Tue, 23 Dec 2014 22:43:45 +0100

haskell-dpkg (0.0.3-4) unstable; urgency=low

  * Move Haskell blurb to the end of the description, reduces the impact
    of #708703

 -- Joachim Breitner <nomeata@debian.org>  Sat, 25 May 2013 23:52:13 +0200

haskell-dpkg (0.0.3-3) unstable; urgency=low

  * Enable compat level 9
  * Use substvars for Haskell description blurbs

 -- Joachim Breitner <nomeata@debian.org>  Fri, 24 May 2013 12:50:30 +0200

haskell-dpkg (0.0.3-2) experimental; urgency=low

  * Depend on haskell-devscripts 0.8.13 to ensure this packages is built
    against experimental
  * Bump standards version, no change

 -- Joachim Breitner <nomeata@debian.org>  Sun, 14 Oct 2012 12:00:43 +0200

haskell-dpkg (0.0.3-1) unstable; urgency=low

  * New upstream version.
    - Tweaked for dpkg API in wheezy.  closes: #680454.

 -- Clint Adams <clint@debian.org>  Thu, 05 Jul 2012 16:50:47 -0600

haskell-dpkg (0.0.2-1) unstable; urgency=low

  * New upstream version.
    - Tweaked for newer API; thanks Iain Lane.  closes: #649518.
  * Bump to Standards-Version 3.9.3.

 -- Clint Adams <clint@debian.org>  Tue, 17 Apr 2012 16:47:33 -0400

haskell-dpkg (0.0.1-2) unstable; urgency=low

  * Sourceful upload to rebuild documentation package

 -- Iain Lane <laney@debian.org>  Thu, 23 Feb 2012 20:12:17 +0000

haskell-dpkg (0.0.1-1) unstable; urgency=low

  * New upstream version.
  * Build-depend on pkg-config.  closes: #643695.
  * Build-depend on c2hs.

 -- Clint Adams <clint@debian.org>  Thu, 29 Sep 2011 22:20:47 -0400

haskell-dpkg (0.0.0-1) unstable; urgency=low

  * Initial release.

 -- Clint Adams <clint@debian.org>  Tue, 27 Sep 2011 14:47:43 -0400
