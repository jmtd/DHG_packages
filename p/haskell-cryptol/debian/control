Source: haskell-cryptol
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders:
 Clint Adams <clint@debian.org>,
Priority: optional
Section: haskell
Rules-Requires-Root: no
Build-Depends:
 cdbs,
 debhelper (>= 10),
 ghc (>= 8.4.3),
 ghc-prof,
 haskell-devscripts (>= 0.13),
 libghc-graphscc-dev (>= 1.0.4),
 libghc-graphscc-prof,
 libghc-base-compat-dev (>= 0.6),
 libghc-base-compat-dev (<< 0.12),
 libghc-base-compat-prof,
 libghc-cryptohash-sha1-dev (>= 0.11),
 libghc-cryptohash-sha1-dev (<< 0.12),
 libghc-cryptohash-sha1-prof,
 libghc-gitrev-dev (>= 1.0),
 libghc-gitrev-prof,
 libghc-graphscc-dev (>= 1.0.4),
 libghc-graphscc-prof,
 libghc-heredoc-dev (>= 0.2),
 libghc-heredoc-prof,
 libghc-monad-control-dev,
 libghc-monad-control-dev (>= 1.0),
 libghc-monad-control-prof,
 libghc-monadlib-dev,
 libghc-monadlib-dev (>= 3.7.2),
 libghc-monadlib-prof,
 libghc-panic-dev (>= 0.3),
 libghc-panic-prof,
 libghc-random-dev (>= 1.0.1),
 libghc-random-prof,
 libghc-sbv-dev (>= 8.1),
 libghc-sbv-prof,
 libghc-simple-smt-dev (>= 0.7.1),
 libghc-simple-smt-prof,
 libghc-strict-dev,
 libghc-strict-prof,
 libghc-tf-random-dev,
 libghc-tf-random-dev (>= 0.5),
 libghc-tf-random-prof,
 libghc-transformers-base-dev (>= 0.4),
 libghc-transformers-base-prof,
 alex,
 happy,
 libghc-ansi-terminal-dev,
 libghc-base-compat-dev,
 libghc-blaze-html-dev,
 libghc-blaze-html-prof,
 libghc-monad-control-dev,
Build-Depends-Indep: ghc-doc,
 libghc-graphscc-doc,
 libghc-base-compat-doc,
 libghc-cryptohash-sha1-doc,
 libghc-gitrev-doc,
 libghc-graphscc-doc,
 libghc-heredoc-doc,
 libghc-monad-control-doc,
 libghc-monadlib-doc,
 libghc-panic-doc,
 libghc-random-doc,
 libghc-sbv-doc,
 libghc-simple-smt-doc,
 libghc-strict-doc,
 libghc-tf-random-doc,
 libghc-transformers-base-doc,
Standards-Version: 4.1.4
Homepage: http://www.cryptol.net/
Vcs-Git: https://salsa.debian.org/haskell-team/DHG_packages.git [p/haskell-cryptol]
Vcs-Browser: https://salsa.debian.org/haskell-team/DHG_packages/tree/master/p/haskell-cryptol
X-Description: domain-specific language of cryptography
 Cryptol is a domain-specific language for specifying cryptographic
 algorithms. A Cryptol implementation of an algorithm resembles its
 mathematical specification more closely than an implementation in
 a general purpose language.

Package: libghc-cryptol-dev
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Conflicts:
 ${haskell:Conflicts},
Provides:
 ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-cryptol-prof
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Conflicts:
 ${haskell:Conflicts},
Provides:
 ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-cryptol-doc
Architecture: all
Section: doc
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Conflicts:
 ${haskell:Conflicts},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: cryptol
Architecture: any
Section: utils
Depends:
 z3,
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Conflicts:
 ${haskell:Conflicts},
Provides:
 ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}
