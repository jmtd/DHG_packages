haskell-gitlib (3.1.2-3) unstable; urgency=medium

  * Remove build dependency on libghc-mtl-dev (provided by ghc-8.4.3)
  * Remove build dependency on libghc-text-dev (provided by ghc-8.4.3)

 -- Ilias Tsitsimpis <iliastsi@debian.org>  Mon, 01 Oct 2018 13:47:31 +0300

haskell-gitlib (3.1.2-2) unstable; urgency=medium

  * Bump debhelper compat level to 10

 -- Ilias Tsitsimpis <iliastsi@debian.org>  Sun, 30 Sep 2018 21:09:44 +0300

haskell-gitlib (3.1.2-1) unstable; urgency=medium

  * Set Rules-Requires-Root to no.
  * New upstream release.
    - Uses unliftio instead of monad-control.  closes: #897513.

 -- Clint Adams <clint@debian.org>  Wed, 30 May 2018 22:24:32 -0400

haskell-gitlib (3.1.1-5) unstable; urgency=medium

  * Patch for no conduit-combinators.

 -- Clint Adams <clint@debian.org>  Sun, 15 Apr 2018 15:54:59 -0400

haskell-gitlib (3.1.1-4) unstable; urgency=medium

  [ Ilias Tsitsimpis ]
  * Change Priority to optional. Since Debian Policy version 4.0.1,
    priority extra has been deprecated.
  * Use the HTTPS form of the copyright-format URL
  * Modify d/watch and Source field in d/copyright to use HTTPS
  * Declare compliance with Debian policy 4.1.1
  * Use salsa.debian.org URLs in Vcs-{Browser,Git} fields

  [ Clint Adams ]
  * Bump to Standards-Version 4.1.4.

 -- Clint Adams <clint@debian.org>  Mon, 09 Apr 2018 20:04:50 -0400

haskell-gitlib (3.1.1-3) unstable; urgency=medium

  * Upload to unstable as part of GHC 8 transition.

 -- Clint Adams <clint@debian.org>  Thu, 27 Oct 2016 18:33:50 -0400

haskell-gitlib (3.1.1-2) experimental; urgency=medium

  [ Dmitry Bogatov ]
  * Use secure (https) uri in Vcs-Git field in 'debian/control'
  * Bump standards version to 3.9.8 (no changes needed)

  [ Clint Adams ]
  * Temporarily build-depend on ghc 8.

 -- Clint Adams <clint@debian.org>  Mon, 17 Oct 2016 20:42:42 -0400

haskell-gitlib (3.1.1-1) unstable; urgency=medium

  * New upstream release

 -- Clint Adams <clint@debian.org>  Fri, 04 Dec 2015 00:03:24 -0500

haskell-gitlib (3.1.0.2-2) unstable; urgency=medium

  * Switch Vcs-Git/Vcs-Browser headers to new location.

 -- Clint Adams <clint@debian.org>  Thu, 03 Dec 2015 14:54:29 -0500

haskell-gitlib (3.1.0.2-1) experimental; urgency=medium

  * New upstream release
  * Bump standards-version to 3.9.6
  * Depend on haskell-devscripts >= 0.10 to ensure that this package
    builds against GHC in experimental

 -- Joachim Breitner <nomeata@debian.org>  Thu, 20 Aug 2015 10:27:41 +0200

haskell-gitlib (3.1.0.1-1) unstable; urgency=medium

  * New upstream release

 -- Joachim Breitner <nomeata@debian.org>  Mon, 29 Jun 2015 10:58:57 +0200

haskell-gitlib (3.1.0-4) unstable; urgency=medium

  * Rebuild due to haskell-devscripts bug affecting the previous

 -- Joachim Breitner <nomeata@debian.org>  Tue, 28 Apr 2015 23:58:33 +0200

haskell-gitlib (3.1.0-3) unstable; urgency=medium

  * Upload to unstable

 -- Joachim Breitner <nomeata@debian.org>  Mon, 27 Apr 2015 11:49:55 +0200

haskell-gitlib (3.1.0-2) experimental; urgency=medium

  * Depend on haskell-devscripts 0.9, found in experimental
  * Do not depend on transformers, which now comes with GHC

 -- Joachim Breitner <nomeata@debian.org>  Sat, 20 Dec 2014 20:31:47 +0100

haskell-gitlib (3.1.0-1) unstable; urgency=medium

  * New upstream release

 -- Joachim Breitner <nomeata@debian.org>  Thu, 19 Jun 2014 13:31:59 -0700

haskell-gitlib (2.2.0.0-1) unstable; urgency=low

  * Initial release.

 -- Clint Adams <clint@debian.org>  Thu, 28 Nov 2013 23:01:02 -0500
