highlighting-kate (0.6.4-6) unstable; urgency=medium

  * Sourceful upload for GHC 8.8

 -- Ilias Tsitsimpis <iliastsi@debian.org>  Mon, 22 Jun 2020 14:16:39 +0300

highlighting-kate (0.6.4-5) unstable; urgency=medium

  * Remove build dependency on libghc-parsec3-dev (provided by ghc-
    8.4.3)

 -- Ilias Tsitsimpis <iliastsi@debian.org>  Wed, 03 Oct 2018 12:06:16 +0300

highlighting-kate (0.6.4-4) unstable; urgency=medium

  * Remove build dependency on libghc-mtl-dev (provided by ghc-8.4.3)

 -- Ilias Tsitsimpis <iliastsi@debian.org>  Mon, 01 Oct 2018 13:33:39 +0300

highlighting-kate (0.6.4-3) unstable; urgency=medium

  [ Clint Adams ]
  * Set Rules-Requires-Root to no.

  [ Ilias Tsitsimpis ]
  * Bump debhelper compat level to 10

 -- Ilias Tsitsimpis <iliastsi@debian.org>  Sun, 30 Sep 2018 21:27:05 +0300

highlighting-kate (0.6.4-2) unstable; urgency=medium

  [ Ilias Tsitsimpis ]
  * Change Priority to optional. Since Debian Policy version 4.0.1,
    priority extra has been deprecated.
  * Use the HTTPS form of the copyright-format URL
  * Modify d/watch and Source field in d/copyright to use HTTPS
  * Declare compliance with Debian policy 4.1.1
  * Use salsa.debian.org URLs in Vcs-{Browser,Git} fields

  [ Clint Adams ]
  * Bump to Standards-Version 4.1.4.

 -- Clint Adams <clint@debian.org>  Mon, 09 Apr 2018 20:05:12 -0400

highlighting-kate (0.6.4-1) unstable; urgency=medium

  * New upstream release

 -- Clint Adams <clint@debian.org>  Sun, 18 Jun 2017 02:41:17 -0400

highlighting-kate (0.6.3-3) unstable; urgency=medium

  * Upload to unstable as part of GHC 8 transition.

 -- Clint Adams <clint@debian.org>  Thu, 27 Oct 2016 18:37:18 -0400

highlighting-kate (0.6.3-2) experimental; urgency=medium

  * Temporarily build-depend on ghc 8.

 -- Clint Adams <clint@debian.org>  Mon, 17 Oct 2016 00:58:00 -0400

highlighting-kate (0.6.3-1) unstable; urgency=medium

  * New upstream release

 -- Clint Adams <clint@debian.org>  Thu, 29 Sep 2016 12:00:25 -0400

highlighting-kate (0.6.2.1-1) unstable; urgency=medium

  * New upstream release

 -- Clint Adams <clint@debian.org>  Tue, 12 Jul 2016 15:55:59 -0400

highlighting-kate (0.6.2-1) unstable; urgency=medium

  [ Dmitry Bogatov ]
  * Use secure (https) uri in Vcs-Git field in 'debian/control'
  * Bump standards version to 3.9.8 (no changes needed)
  * Convert `debian/copyright' to dep5 format

  [ Joachim Breitner ]
  * New upstream release

 -- Joachim Breitner <nomeata@debian.org>  Mon, 30 May 2016 17:21:09 +0200

highlighting-kate (0.6.1-1) unstable; urgency=medium

  * New upstream release

 -- Clint Adams <clint@debian.org>  Sun, 10 Jan 2016 23:17:56 -0500

highlighting-kate (0.6-2) unstable; urgency=medium

  * Switch Vcs-Git/Vcs-Browser headers to new location.

 -- Clint Adams <clint@debian.org>  Thu, 03 Dec 2015 14:55:21 -0500

highlighting-kate (0.6-1) experimental; urgency=medium

  * New upstream release
  * Bump standards-version to 3.9.6
  * Depend on haskell-devscripts >= 0.10 to ensure that this package
    builds against GHC in experimental

 -- Joachim Breitner <nomeata@debian.org>  Thu, 20 Aug 2015 10:29:12 +0200

highlighting-kate (0.5.12-2) unstable; urgency=medium

  * Upload to unstable

 -- Joachim Breitner <nomeata@debian.org>  Mon, 27 Apr 2015 11:55:53 +0200

highlighting-kate (0.5.12-1) experimental; urgency=medium

  * New upstream release

 -- Joachim Breitner <nomeata@debian.org>  Mon, 06 Apr 2015 11:49:27 +0200

highlighting-kate (0.5.11.1-1) experimental; urgency=medium

  * New upstream release

 -- Joachim Breitner <nomeata@debian.org>  Tue, 30 Dec 2014 12:37:18 +0100

highlighting-kate (0.5.8.5-3) experimental; urgency=medium

  * Depend on haskell-devscripts 0.9, found in experimental

 -- Joachim Breitner <nomeata@debian.org>  Sat, 20 Dec 2014 17:12:57 +0100

highlighting-kate (0.5.8.5-2) unstable; urgency=medium

  * New upstream release

 -- Joachim Breitner <nomeata@debian.org>  Tue, 05 Aug 2014 01:01:50 +0200

highlighting-kate (0.5.8.2-1) unstable; urgency=medium

  * New upstream version.

 -- Clint Adams <clint@debian.org>  Fri, 06 Jun 2014 08:17:50 -0400

highlighting-kate (0.5.7-1) unstable; urgency=medium

  * New upstream version.

 -- Clint Adams <clint@debian.org>  Fri, 09 May 2014 21:28:08 -0400

highlighting-kate (0.5.6.1-1) unstable; urgency=medium

  * New upstream release

 -- Joachim Breitner <nomeata@debian.org>  Fri, 07 Mar 2014 13:02:05 +0100

highlighting-kate (0.5.5.1-1) unstable; urgency=low

  [ Joachim Breitner ]
  * Adjust watch file to new hackage layout

  [ Clint Adams ]
  * New upstream version.

 -- Clint Adams <clint@debian.org>  Sun, 24 Nov 2013 19:45:33 -0500

highlighting-kate (0.5.3.8-2) unstable; urgency=low

  * Enable compat level 9
  * Use substvars for Haskell description blurbs

 -- Joachim Breitner <nomeata@debian.org>  Fri, 24 May 2013 12:52:44 +0200

highlighting-kate (0.5.3.8-1) experimental; urgency=low

  * New upstream release

 -- Joachim Breitner <nomeata@debian.org>  Sun, 10 Mar 2013 00:59:29 +0100

highlighting-kate (0.5.1-2) experimental; urgency=low

  * Depend on haskell-devscripts 0.8.13 to ensure this packages is built
    against experimental
  * Bump standards version, no change

 -- Joachim Breitner <nomeata@debian.org>  Sun, 14 Oct 2012 12:02:39 +0200

highlighting-kate (0.5.1-1) unstable; urgency=low

  * New upstream release

 -- Joachim Breitner <nomeata@debian.org>  Sat, 16 Jun 2012 12:51:47 +0200

highlighting-kate (0.5.0.5-1) unstable; urgency=low

  * New upstream release
  * DM-Upload-Allowed: yes
  * New debian/control file like highlighting-kate.cabal
  * Fix spelling of PHP and SQL in description (Closes: #630158)

 -- Kiwamu Okabe <kiwamu@debian.or.jp>  Tue, 28 Feb 2012 15:25:47 +0900

highlighting-kate (0.2.9-1) unstable; urgency=low

  [ Marco Silva ]
  * Use ghc instead of ghc6

  [ Joachim Breitner ]
  * New upstream release
  * Install documentation into standard location

 -- Joachim Breitner <nomeata@debian.org>  Fri, 08 Apr 2011 15:29:11 +0530

highlighting-kate (0.2.7.1-1) unstable; urgency=low

  * Priority: extra
  * New upstream version (Closes: #591168)

 -- Joachim Breitner <nomeata@debian.org>  Mon, 02 Aug 2010 16:42:27 +0200

highlighting-kate (0.2.6.2-1) unstable; urgency=low

  * New upstream release, even better build time improvements

 -- Joachim Breitner <nomeata@debian.org>  Sun, 07 Mar 2010 13:17:08 +0100

highlighting-kate (0.2.6.1-1) unstable; urgency=low

  * New upstream release which incorporates our changes
  * Remove debian/patches/Fix-source-encoding.patch, fixed upstream

 -- Joachim Breitner <nomeata@debian.org>  Sat, 06 Mar 2010 23:03:27 +0100

highlighting-kate (0.2.5-5) unstable; urgency=low

  * Massively reduce compile time by tricking ghc not to inline a large word
    list

 -- Joachim Breitner <nomeata@debian.org>  Sat, 06 Mar 2010 18:20:27 +0100

highlighting-kate (0.2.5-4) unstable; urgency=low

  [ Marco Túlio Gontijo e Silva ]
  * debian/source/format: Use 3.0 (quilt).

  [ Joachim Breitner ]
  * debian/patches/Fix-source-encoding.patch: Haskell source files ought to be
    UTF8. (Closes: #570964)

 -- Joachim Breitner <nomeata@debian.org>  Mon, 22 Feb 2010 15:18:01 +0100

highlighting-kate (0.2.5-3) unstable; urgency=low

  [ Marco Túlio Gontijo e Silva ]
  * debian/watch: Use format that works for --download-current-version.
  * debian/watch: Add .tar.gz to downloaded filename.
  * debian/watch: Include package name in downloaded .tar.gz.
  * debian/watch: Remove spaces, since they're not allowed by uscan.
  * debian/control: Add field Provides: ${haskell:Provides} to -dev and
    -prof packages.
  * debian/control: Update fields XS-Vcs-* to Vcs- and pointed them to
    the Debian Haskell Group repository.
  * debian/control: Use simpler version of Vcs-Browser.
  * debian/control: Use comma in the beginning of the line.
  * debian/control: Remove dependency in hscolour, since it's now a
    dependency of haskell-devscripts.
  * debian/control: Remove haddock from Build-Depends:, since it's now a
    Depends: of haskell-devscripts.
  * debian/control: Bump Standards-Version: to 3.8.4, no changes needed.
  * debian/control: Use standard name for -doc package.
  * debian/control: Add Depends: ${misc:Depends} to -doc.
  * debian/control: Add ${haskell:Recommends} and Suggests, and Depends on
    -doc.

  [ Joachim Breitner ]
  * Bump dependency versions

 -- Marco Túlio Gontijo e Silva <marcot@debian.org>  Fri, 12 Feb 2010 14:18:25 -0200

highlighting-kate (0.2.5-2) unstable; urgency=low

  * Copy a rule from hlibrary.mk to cater for the non-standard documentation
    package name

 -- Joachim Breitner <nomeata@debian.org>  Wed, 29 Jul 2009 02:56:50 +0200

highlighting-kate (0.2.5-1) unstable; urgency=low

  * Take over by the Debian Haskell Group. Thanks to Recai for his previous
    maintenance.
  * New upstream version (Closes: #507338)
  * Redo packaging accoring to currenct practice

 -- Joachim Breitner <nomeata@debian.org>  Wed, 29 Jul 2009 02:17:20 +0200

highlighting-kate (0.2.1-3) unstable; urgency=low

  * Brown paper bag release.  Add libghc6-parsec-prof to Build-Depends.
    Closes: #467589

 -- Recai Oktaş <roktas@debian.org>  Fri, 29 Feb 2008 00:09:00 +0200

highlighting-kate (0.2.1-2) unstable; urgency=low

  * Add libghc6-parsec-dev to Build-Depends.  Closes: #467589

 -- Recai Oktaş <roktas@debian.org>  Wed, 27 Feb 2008 21:37:43 +0200

highlighting-kate (0.2.1-1) unstable; urgency=low

  * Initial release.  Closes: #464920

 -- Recai Oktaş <roktas@debian.org>  Sat, 09 Feb 2008 20:33:08 +0200
