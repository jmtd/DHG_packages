Source: highlighting-kate
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders:
 Joachim Breitner <nomeata@debian.org>,
 Kiwamu Okabe <kiwamu@debian.or.jp>,
Priority: optional
Section: haskell
Rules-Requires-Root: no
Build-Depends:
 cdbs,
 debhelper (>= 10),
 ghc (>= 8.4.3),
 ghc-prof,
 haskell-devscripts (>= 0.13),
 libghc-blaze-html-dev (>= 0.4.2),
 libghc-blaze-html-dev (<< 0.10),
 libghc-blaze-html-prof,
 libghc-diff-dev,
 libghc-pcre-light-dev (<< 0.5),
 libghc-pcre-light-dev (>= 0.4),
 libghc-pcre-light-prof,
 libghc-utf8-string-dev,
 libghc-utf8-string-prof,
Build-Depends-Indep:
 ghc-doc,
 libghc-blaze-html-doc,
 libghc-pcre-light-doc,
 libghc-utf8-string-doc,
Standards-Version: 4.5.0
Homepage: https://github.com/jgm/highlighting-kate
Vcs-Browser: https://salsa.debian.org/haskell-team/DHG_packages/tree/master/p/highlighting-kate
Vcs-Git: https://salsa.debian.org/haskell-team/DHG_packages.git [p/highlighting-kate]

Package: libghc-highlighting-kate-dev
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: syntax highlighting library based on Kate syntax descriptions${haskell:ShortBlurb}
 Highlighting-kate is a syntax highlighting library with support for over
 50 languages. The syntax parsers are automatically generated from Kate syntax
 descriptions (<http://kate-editor.org/>), so any syntax supported by Kate can
 be added.
 .
 Currently the following languages are supported: Ada, Asp, Awk, Bash, Bibtex,
 C, Cmake, Coldfusion, Commonlisp, Cpp, Css, D, Djangotemplate, Doxygen, Dtd,
 Eiffel, Erlang, Fortran, Haskell, Html, Java, Javadoc, Javascript, Json,
 Latex, Lex, LiterateHaskell, Lua, Makefile, Matlab, Mediawiki, Modula3, Nasm,
 Objectivec, Ocaml, Pascal, Perl, PHP, Postscript, Prolog, Python, Rhtml, Ruby,
 Scala, Scheme, Sgml, SQL, MySQL, PostgreSQL, Tcl, Texinfo, Xml, Xslt,
 Yacc.
 .
 ${haskell:Blurb}

Package: libghc-highlighting-kate-prof
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: highlighting-kate library with profiling enabled${haskell:ShortBlurb}
 Highlighting-kate is a syntax highlighting library with support for over
 50 languages. The syntax parsers are automatically generated from Kate syntax
 descriptions (<http://kate-editor.org/>), so any syntax supported by Kate can
 be added.
 .
 Currently the following languages are supported: Ada, Asp, Awk, Bash, Bibtex,
 C, Cmake, Coldfusion, Commonlisp, Cpp, Css, D, Djangotemplate, Doxygen, Dtd,
 Eiffel, Erlang, Fortran, Haskell, Html, Java, Javadoc, Javascript, Json,
 Latex, Lex, LiterateHaskell, Lua, Makefile, Matlab, Mediawiki, Modula3, Nasm,
 Objectivec, Ocaml, Pascal, Perl, PHP, Postscript, Prolog, Python, Rhtml, Ruby,
 Scala, Scheme, Sgml, SQL, MySQL, PostgreSQL, Tcl, Texinfo, Xml, Xslt,
 Yacc.
 .
 ${haskell:Blurb}

Package: libghc-highlighting-kate-doc
Architecture: all
Section: doc
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Description: library documentation for highlighting-kate${haskell:ShortBlurb}
 Highlighting-kate is a syntax highlighting library with support for over
 50 languages. The syntax parsers are automatically generated from Kate syntax
 descriptions (<http://kate-editor.org/>), so any syntax supported by Kate can
 be added.
 .
 Currently the following languages are supported: Ada, Asp, Awk, Bash, Bibtex,
 C, Cmake, Coldfusion, Commonlisp, Cpp, Css, D, Djangotemplate, Doxygen, Dtd,
 Eiffel, Erlang, Fortran, Haskell, Html, Java, Javadoc, Javascript, Json,
 Latex, Lex, LiterateHaskell, Lua, Makefile, Matlab, Mediawiki, Modula3, Nasm,
 Objectivec, Ocaml, Pascal, Perl, PHP, Postscript, Prolog, Python, Rhtml, Ruby,
 Scala, Scheme, Sgml, SQL, MySQL, PostgreSQL, Tcl, Texinfo, Xml, Xslt,
 Yacc.
 .
 ${haskell:Blurb}
