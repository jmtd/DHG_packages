Source: haskell-repa
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders:
 Denis Laxalde <denis@laxalde.org>,
Priority: optional
Section: haskell
Rules-Requires-Root: no
Build-Depends:
 cdbs,
 debhelper (>= 10),
 ghc (>= 8),
 ghc-prof,
 haskell-devscripts (>= 0.13),
 libghc-quickcheck2-dev (>= 2.8),
 libghc-quickcheck2-dev (<< 2.14),
 libghc-quickcheck2-prof,
 libghc-vector-dev (<< 0.13),
 libghc-vector-dev (>= 0.11),
 libghc-vector-dev (<< 0.13),
 libghc-vector-prof,
Build-Depends-Indep:
 ghc-doc,
 libghc-quickcheck2-doc,
 libghc-vector-doc,
Standards-Version: 4.5.0
Homepage: http://repa.ouroborus.net
Vcs-Browser: https://salsa.debian.org/haskell-team/DHG_packages/tree/master/p/haskell-repa
Vcs-Git: https://salsa.debian.org/haskell-team/DHG_packages.git [p/haskell-repa]

Package: libghc-repa-dev
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: Regular parallel arrays for Haskell${haskell:ShortBlurb}
 Repa provides high performance, regular, multi-dimensional, shape polymorphic
 parallel arrays. All numeric data is stored unboxed. Functions written with
 the Repa combinators are automatically parallel provided you supply +RTS -Nn
 on the command line when running the program.
 .
 ${haskell:Blurb}

Package: libghc-repa-prof
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: Regular parallel arrays for Haskell${haskell:ShortBlurb}
 Repa provides high performance, regular, multi-dimensional, shape polymorphic
 parallel arrays. All numeric data is stored unboxed. Functions written with
 the Repa combinators are automatically parallel provided you supply +RTS -Nn
 on the command line when running the program.
 .
 ${haskell:Blurb}

Package: libghc-repa-doc
Architecture: all
Section: doc
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Description: Regular parallel arrays for Haskell${haskell:ShortBlurb}
 Repa provides high performance, regular, multi-dimensional, shape polymorphic
 parallel arrays. All numeric data is stored unboxed. Functions written with
 the Repa combinators are automatically parallel provided you supply +RTS -Nn
 on the command line when running the program.
 .
 ${haskell:Blurb}
