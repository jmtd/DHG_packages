Source: darcs
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Priority: optional
Section: haskell
Rules-Requires-Root: no
Build-Depends:
 haskell-devscripts (>= 0.13),
 bash-completion (>= 1:1.1),
 cdbs,
 debhelper (>= 10),
 libcurl4-gnutls-dev (>= 7.19.1),
 ghc (>= 8.4.3),
 ghc-prof,
 libghc-http-dev (>= 1:4000.2.20),
 libghc-http-dev (<< 1:4000.4),
 libghc-http-prof,
 libghc-async-dev (>= 2.0.2),
 libghc-async-dev (<< 2.3),
 libghc-async-prof,
 libghc-attoparsec-dev (>= 0.13.0.1),
 libghc-attoparsec-dev (<< 0.14),
 libghc-base16-bytestring-dev (>= 0.1),
 libghc-base16-bytestring-dev (<< 0.2),
 libghc-base16-bytestring-prof,
 libghc-cryptohash-dev (>= 0.11),
 libghc-cryptohash-dev (<< 0.12),
 libghc-data-ordlist-dev (>= 0.4),
 libghc-data-ordlist-dev (<< 0.5),
 libghc-data-ordlist-prof,
 libghc-fgl-dev (>= 5.5.2.3),
 libghc-fgl-dev (<< 5.8),
 libghc-fgl-prof,
 libghc-graphviz-dev (>= 2999.18.1),
 libghc-graphviz-dev (<< 2999.20.1),
 libghc-graphviz-prof,
 libghc-hashable-dev (>= 1.2.3.3),
 libghc-hashable-dev (<< 1.4),
 libghc-hashable-prof,
 libghc-html-dev (>= 1.0.1.2),
 libghc-html-dev (<< 1.1),
 libghc-html-prof,
 libghc-mmap-dev (>= 0.5.9),
 libghc-mmap-dev (<< 0.6),
 libghc-mmap-prof,
 libghc-network-dev (>= 2.6),
 libghc-network-dev (<< 3.2),
 libghc-network-prof,
 libghc-network-uri-dev (>= 2.6),
 libghc-network-uri-dev (<< 2.7),
 libghc-network-uri-prof,
 libghc-old-time-dev (>= 1.1.0.3),
 libghc-old-time-dev (<< 1.2),
 libghc-old-time-prof,
 libghc-random-dev (>= 1.1),
 libghc-random-dev (<< 1.2),
 libghc-regex-applicative-dev (>= 0.2),
 libghc-regex-applicative-dev (<< 0.4),
 libghc-regex-compat-tdfa-dev (>= 0.95.1),
 libghc-regex-compat-tdfa-dev (<< 0.96),
 libghc-regex-compat-tdfa-prof,
 libghc-sandi-dev (>= 0.4),
 libghc-sandi-dev (<< 0.6),
 libghc-sandi-prof,
 libghc-tar-dev (>= 0.5),
 libghc-tar-dev (<< 0.6),
 libghc-tar-prof,
 libghc-unix-compat-dev (>= 0.4.2),
 libghc-unix-compat-dev (<< 0.6),
 libghc-unix-compat-prof,
 libghc-utf8-string-dev (>= 1),
 libghc-utf8-string-dev (<< 1.1),
 libghc-utf8-string-prof,
 libghc-vector-dev (>= 0.11),
 libghc-vector-dev (<< 0.13),
 libghc-vector-prof,
 libghc-zip-archive-dev (>= 0.3),
 libghc-zip-archive-dev (<< 0.5),
 libghc-zip-archive-prof,
 libghc-zlib-dev (>= 0.6.1.2),
 libghc-zlib-dev (<< 0.7.0.0),
 libghc-zlib-prof,
 pkg-config,
Build-Depends-Indep:
 ghc-doc,
 libghc-async-doc,
 libghc-attoparsec-doc,
 libghc-base16-bytestring-doc,
 libghc-cryptohash-doc,
 libghc-data-ordlist-doc,
 libghc-fgl-doc,
 libghc-graphviz-doc,
 libghc-hashable-doc,
 libghc-html-doc,
 libghc-http-doc,
 libghc-mmap-doc,
 libghc-network-doc,
 libghc-network-uri-doc,
 libghc-old-time-doc,
 libghc-random-doc,
 libghc-regex-applicative-doc,
 libghc-regex-compat-tdfa-doc,
 libghc-sandi-doc,
 libghc-tar-doc,
 libghc-unix-compat-doc,
 libghc-utf8-string-doc,
 libghc-vector-doc,
 libghc-zip-archive-doc,
 libghc-zlib-doc,
Standards-Version: 4.5.0
Homepage: http://darcs.net/
Vcs-Browser: https://salsa.debian.org/haskell-team/DHG_packages/tree/master/p/darcs
Vcs-Git: https://salsa.debian.org/haskell-team/DHG_packages.git [p/darcs]

Package: darcs
Architecture: any
Multi-Arch: foreign
Section: vcs
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: distributed, interactive, smart revision control system
 Darcs is a free, open source revision control system.  It is:
 .
  * Distributed: Every user has access to the full command set,
    removing boundaries between server and client or committer and
    non-committers.
  * Interactive: Darcs is easy to learn and efficient to use because
    it asks you questions in response to simple commands, giving you
    choices in your work flow. You can choose to record one change in
    a file, while ignoring another. As you update from upstream, you
    can review each patch name, even the full "diff" for interesting
    patches.
  * Smart: Originally developed by physicist David Roundy, darcs is
    based on a unique algebra of patches. This smartness lets you
    respond to changing demands in ways that would otherwise not be
    possible.
