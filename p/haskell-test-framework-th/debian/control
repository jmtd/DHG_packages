Source: haskell-test-framework-th
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders:
 Kiwamu Okabe <kiwamu@debian.or.jp>,
 Clint Adams <clint@debian.org>,
Priority: optional
Section: haskell
Rules-Requires-Root: no
Build-Depends:
 cdbs,
 debhelper (>= 10),
 ghc (>= 8),
 ghc-prof,
 haskell-devscripts (>= 0.13),
 libghc-language-haskell-extract-dev (>= 0.2),
 libghc-language-haskell-extract-prof,
 libghc-regex-posix-dev,
 libghc-regex-posix-prof,
 libghc-src-exts-dev,
 libghc-src-exts-prof,
 libghc-test-framework-dev,
 libghc-test-framework-prof,
Build-Depends-Indep:
 ghc-doc,
 libghc-language-haskell-extract-doc,
 libghc-regex-posix-doc,
 libghc-src-exts-doc,
 libghc-test-framework-doc,
Standards-Version: 4.5.0
Homepage: https://github.com/finnsson/test-generator
Vcs-Browser: https://salsa.debian.org/haskell-team/DHG_packages/tree/master/p/haskell-test-framework-th
Vcs-Git: https://salsa.debian.org/haskell-team/DHG_packages.git [p/haskell-test-framework-th]

Package: libghc-test-framework-th-dev
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: Automagically generate the {HUnit,Quickcheck}-bulk-code
 test-framework-th contains two interesting functions:
 defaultMainGenerator and testGroupGenerator.
 .
 defaultMainGenerator will extract all functions beginning with
 "case_" or "prop_" in the module and put them in a testGroup.
 .
 testGroupGenerator is like defaultMainGenerator but without defaultMain.
 It is useful if you need a function for the testgroup
 (e.g. if you want to be able to call the testgroup from another module).
 .
 This package contains the normal library files.

Package: libghc-test-framework-th-prof
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: Automagically generate the {HUnit,Quickcheck}-bulk-code; profiling libraries
 test-framework-th contains two interesting functions:
 defaultMainGenerator and testGroupGenerator.
 .
 defaultMainGenerator will extract all functions beginning with
 "case_" or "prop_" in the module and put them in a testGroup.
 .
 testGroupGenerator is like defaultMainGenerator but without defaultMain.
 It is useful if you need a function for the testgroup
 (e.g. if you want to be able to call the testgroup from another module).
 .
 This package contains the libraries compiled with profiling enabled.

Package: libghc-test-framework-th-doc
Architecture: all
Section: doc
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Description: Automagically generate the {HUnit,Quickcheck}-bulk-code; documentation
 test-framework-th contains two interesting functions:
 defaultMainGenerator and testGroupGenerator.
 .
 defaultMainGenerator will extract all functions beginning with
 "case_" or "prop_" in the module and put them in a testGroup.
 .
 testGroupGenerator is like defaultMainGenerator but without defaultMain.
 It is useful if you need a function for the testgroup
 (e.g. if you want to be able to call the testgroup from another module).
 .
 This package contains the documentation files.
