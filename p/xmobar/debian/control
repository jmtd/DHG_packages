Source: xmobar
Section: x11
Priority: optional
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders: Apollon Oikonomopoulos <apoikos@debian.org>,
           Aggelos Avgerinos <evaggelos.avgerinos@gmail.com>,
Standards-Version: 4.5.0
Build-Depends: debhelper-compat (=12),
               ghc,
               libghc-alsa-core-dev (>= 0.5),
               libghc-alsa-mixer-dev (>= 0.3),
               libghc-dbus-dev [linux-any],
               libghc-extensible-exceptions-dev,
               libghc-hinotify-dev [linux-any],
               libghc-http-conduit-dev,
               libghc-http-dev (>= 1:4000),
               libghc-iwlib-dev (>= 0.1.0) [linux-any],
               libghc-mtl-dev,
               libghc-parsec-numbers-dev,
               libghc-parsec3-dev,
               libghc-regex-compat-dev,
               libghc-stm-dev (>= 2.3),
               libghc-x11-dev (>= 1.6.0),
               libghc-x11-xft-dev (>= 0.2),
               libiw-dev [linux-any],
               libxpm-dev,
Vcs-Browser: https://salsa.debian.org/haskell-team/DHG_packages/tree/master/p/xmobar
Vcs-Git: https://salsa.debian.org/haskell-team/DHG_packages.git [p/xmobar]
Homepage: http://projects.haskell.org/xmobar/

Package: xmobar
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends},
Suggests: xmonad,
Description: lightweight status bar for X11 window managers
 xmobar is a lightweight text-based status bar for X11 desktops written in
 Haskell and designed to work with (but not limited to) xmonad. It has a
 variety of plugins allowing the display of system-related data and an
 extensible architecture that makes it easy to write one's own plugins. xmobar
 supports displaying UTF-8 text and using TrueType fonts via Xft.
